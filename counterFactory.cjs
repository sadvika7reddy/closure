function countFactory(){
    let number1=0;
    let number2=0;
    function increment(){
        number1=number1+1;
        return number1;
    }
    function decrement(){
        number2=number2-1;
        return number2;
    }
    return {increment,decrement}
}
module.exports = countFactory;