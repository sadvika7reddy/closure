function limitFunctionCallcount(callBack,n){
    if(typeof callBack!='function'||typeof n!=='number' ||n<=0){
        throw new Error('callBack must be function and n should be number')
    }
    let count=0;
    function Call(...args){
        
        if(count<n){
            count=count+1;
            console.log(count);
            return callBack(...args);
        }
        else{
            console.log("call can't run");
            return null;
        }

    }
    return Call;
}

module.exports = limitFunctionCallcount;