function cacheFunction(callBack){
    if(typeof callBack!='function'){
        throw new Error('callBack must be function')
    }
       let object={};
    function cache(...name){
        const arg=JSON.stringify(name)
        if(!object[arg]){       
            let result=callBack(...name);
            object[arg]=result;
            console.log(object[arg],"object");
            return result;
        }
        else{
            return object[arg];  
        }

    }
    
    return cache;
}
module.exports = cacheFunction;

